#!/bin/bash

# This bash script is used to call the peaks using macs2 package: 
# The input should be the directory of the cell lines directories.
# Each cell line directory contains all bed files that we want to get their peaks
while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -i|--INPUT)
    INPUT="$2"
    shift # past argument
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

echo INPUT  = "${INPUT}"
if [[ "${INPUT}" != */ ]];then

     INPUT="${INPUT}/"
fi
echo FILES:
#/usr/bin/samtools sort $hm_file > "${hm_file::-4}"_sorted
#/usr/bin/samtools index "${hm_file::-4}"_sorted

for cell_line in "${INPUT}"*
do
    #echo "$cell_line"
    for hm_file in "$cell_line"/*
    do
        if [[ "$hm_file" == *bed ]]; then
            echo "$hm_file"
                        macs2 callpeak -t "$hm_file" -g hs --nomodel --nolambda -n ${hm_file::-4}
        fi
    done
    echo --------------------------------------
done
