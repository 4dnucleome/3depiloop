3DEpiLoop for chromatin Interactions prediction - Version 0.1

Requirements:
1- R: https://cran.r-project.org/
2- R Packages: (dplyr, randomForest, caret, dplyr, tidyr, ROCR, sqldf).

Notes:
1- To run the script you just need to change the path in the home_dir variable to the current floder contain the code in the local computer.
2- Each folder contains scripts to run 3DEpiLoop with a simple example.
3- For Physical Interactions (Hi-C loops, CTCF/RNAPII ChIA-PET) Run the scripts in this order:
	assignPeaksToGenome.R
	prepareSegments.R
	createPairs.R
	test1.R
4- For HiC Heatmap Interactions Run the scripts in this order:
	assignPeaksToGenome.R
	createPairs.R
	assignHicHeatmapsToPairs.R
	test1.R

5- macs2_peaks_calling.bash : call peaks using MACS2 from bed files.