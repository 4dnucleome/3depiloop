
rm(list = ls())
library("dplyr")

# GLOBAL DECLARATIONS
cell_line <- "GM12878"

resulotion_window <- 1000
home_dir <- "D:/Center Of New Technology/Scientific Reports/code/"


tads_file <- paste0(home_dir, "GeneralInfo/tads.txt")
chromsizefile <- paste0(home_dir, "GeneralInfo/hg19chromsize.txt")

output_dir <- paste0(cell_line, "_CTCF_ChIAPET")

if (!file.exists(paste0(home_dir, "output_data/", output_dir))){
  dir.create(file.path(paste0(home_dir, "output_data/"), output_dir), showWarnings = FALSE)
}

filter_background_data = FALSE
# Genome Information
cell_lines_list <- c(cell_line)

features_list <- c("H2AZ", "H3K4me1", "H3K4me2", "H3K4me3", "H3K9ac", "H3K9me3", "H3K27ac", "H3K27me3", "H3K36me3", "H3K79me2",  "H4K20me1", "EZH2", "POL2", "CTCF", "RAD21", "SMC3", "ZNF143", "NRS")
chromosomes_size = read.table(chromsizefile, header=F, sep="\t", quote="");
tads = read.table(tads_file, header=F, sep="\t", quote="");
colnames(tads) <- c("chrom_tad", "start", "end")



#FUNCTIONS SCOPE
check_if_file_exist <- function(histone_modifications, available_files) {
  found = FALSE
  for(i in 1:length(available_files)){
    if (grepl(tolower(histone_modifications), tolower(available_files[i])))
    {
      found = i
      break
    }
  }
  if(found)
    available_files[found]
  else
    "NOT AVAILABLE"
}

assign_tad_ranges <- function (tad_range, tad_start_vector, tad_end_vector, covered_resolution){
  tad_start_vector[ceiling(tad_range[1] / covered_resolution):ceiling((tad_range[2]) / covered_resolution)] <- tad_range[1]
  tad_end_vector[ceiling(tad_range[1] / covered_resolution):ceiling((tad_range[2]) / covered_resolution)] <- tad_range[2]
}


filter_cond <- paste(features_list, collapse='_Height != 0 | ' )
filter_cond <- paste(filter_cond, '_Height != 0  ')

studied_chroms <- c("chr1", "chr2")

for(curr_chrom in studied_chroms){
  print(paste0("Chromosome: ", curr_chrom))
  curr_chrom_length = chromosomes_size[which(chromosomes_size$V1 == curr_chrom), 2]
  chrom_tads <- filter(tads, chrom_tad == curr_chrom)
  for (cell_lines_counter  in 1:length(cell_lines_list)){
    print (paste0("Cell Line: ", cell_lines_list[cell_lines_counter]))
    available_files <- list.files(paste0(home_dir, "input_data/", cell_lines_list[cell_lines_counter], "/"), pattern = "\\.xls$")
    final_files <- lapply(features_list, check_if_file_exist, available_files=available_files)
    if("NOT AVAILABLE" %in% final_files)
    {
      for (k in 1:length(features_list))
        print(paste0(features_list[k], " -> ", final_files[k]))
      stop(paste0("SOME HISTONE MODIFICATION FILES ARE MISSED FOR THE CELL LINE : ", cell_lines_list[cell_lines_counter]))
    }
    print("Files: ")
    for (k in 1:length(features_list))
      print(paste0(features_list[k], " -> ", final_files[k]))
    
    
    start_pos_vec <- seq(0, curr_chrom_length + 5000, resulotion_window)
    tads_start_vector <- numeric(length(start_pos_vec))
    tads_end_vector <- numeric(length(start_pos_vec))
    for (tad_counter in 1:nrow(chrom_tads)){
      tads_start_vector[ceiling(chrom_tads[tad_counter, 2] / resulotion_window):ceiling((chrom_tads[tad_counter, 3]) / resulotion_window)] <- chrom_tads[tad_counter, 2]
      tads_end_vector  [ceiling(chrom_tads[tad_counter, 2] / resulotion_window):ceiling((chrom_tads[tad_counter, 3]) / resulotion_window)] <- chrom_tads[tad_counter, 3]
    }
    
    d <- data.frame(start_pos_vec)
    d <- cbind(d, cell_lines_list[cell_lines_counter])
    d <- cbind(d, curr_chrom)
    d <- cbind(d, tads_start_vector)
    d <- cbind(d, tads_end_vector)
    colnames(d) <- c("Position", "CellType","Chrom", "TAD_Start", "TAD_End")
    for (hm_file_counter in 1: length(final_files)){
      #hm_file_counter = 1
      peaks_file_path = paste0(home_dir, "input_data/", cell_lines_list[cell_lines_counter], "/", final_files[hm_file_counter])
      
      print (peaks_file_path)
      peaks = read.table(peaks_file_path, header = TRUE, sep = "\t")
      colnames(peaks)[[7]] <- "pvalue"
      colnames(peaks)[[9]] <- "qvalue"
      peaks <- peaks[, 1:9]
      peaks$chr <- as.factor(as.character(peaks$chr))
      peaks$start <- as.numeric(as.character(peaks$start))
      peaks$end <- as.numeric(as.character(peaks$end))
      peaks$length <- as.numeric(as.character(peaks$length))
      peaks$abs_summit <- as.numeric(as.character(peaks$abs_summit))
      peaks$pileup <- as.numeric(as.character(peaks$pileup))
      peaks$pvalue <- as.numeric(as.character(peaks$pvalue))
      peaks$fold_enrichment <- as.numeric(as.character(peaks$fold_enrichment))
      peaks$qvalue <- as.numeric(as.character(peaks$qvalue))
      #summary(peaks)
      #peaks[1,]
      peaks <- filter(peaks, chr == curr_chrom)
      hm_peaks_height_vector <- numeric(length(start_pos_vec))
      hm_peaks_summit_distance_vector <- numeric(length(start_pos_vec))
      hm_peaks_summit_distance_vector <- hm_peaks_summit_distance_vector + curr_chrom_length 
      #summary(peaks)
      for (row_counter in 1:nrow(peaks))
      {
        start_in_genome <- ceiling(peaks[row_counter, 2] / resulotion_window);
        end_in_genome <- ceiling(peaks[row_counter, 3] / resulotion_window);
        for (inner_counter in start_in_genome:end_in_genome)
        {
          hm_peaks_height_vector[[inner_counter]] <- ifelse(hm_peaks_height_vector[[inner_counter]] == 0, 
                                                            peaks[row_counter, 7], 
                                                            max(hm_peaks_height_vector[[inner_counter]], peaks[row_counter, 7]) )
          
          hm_peaks_summit_distance_vector[[inner_counter]] <- ifelse(hm_peaks_summit_distance_vector[[inner_counter]] == 0, 
                                                                     abs(peaks[row_counter, 5] - (inner_counter * resulotion_window - ceiling(0.5 * resulotion_window))), 
                                                                     min(abs(peaks[row_counter, 5] - (inner_counter * resulotion_window - ceiling(0.5 * resulotion_window))), hm_peaks_summit_distance_vector[[inner_counter]])
          )  
        }
      }
      mx <- max(hm_peaks_height_vector)
      mn <- min(hm_peaks_height_vector)
      hm_peaks_height_vector <- hm_peaks_height_vector - mn
      hm_peaks_height_vector <- hm_peaks_height_vector / (mx - mn) * 100
      d <- cbind(d, hm_peaks_height_vector)
      d <- cbind(d, hm_peaks_summit_distance_vector)
      colnames(d)[[length(colnames(d)) - 1]] <- paste0(features_list[hm_file_counter], "_Height")
      colnames(d)[[length(colnames(d))]] <- paste0(features_list[hm_file_counter], "_Summit")
    }
    
    final_data <- d
    # Change writing to several files -> Not tested  Ziad
    print(paste0("WRITE TO: ", home_dir, "output_data/",output_dir,"/segments_", cell_lines_list[cell_lines_counter], "_", curr_chrom, ".RData"))
    saveRDS(final_data, paste0(home_dir, "output_data/",output_dir,"/segments_", cell_lines_list[cell_lines_counter], "_", curr_chrom, ".RData"))
    final_data <- final_data[0,]
  }
  
}

rm(list = ls()) 