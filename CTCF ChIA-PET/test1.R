rm(list = ls())
library(randomForest)
library(ROCR)
library(caret)
library(dplyr)
library(tidyr)
home_dir <- "D:/Center Of New Technology/Scientific Reports/code/"
cell_type <- "GM12878"
train_chr <- "chr1"
test_chr <- "chr2"

pairs_data <- readRDS(paste0(home_dir, "output_data/",cell_type,"_CTCF_ChIAPET/pairs_", train_chr, "_",cell_type,".RData" ))
summary(pairs_data)
pairs_data$Y <- as.factor(ifelse(as.character(pairs_data$Y) == "0", "2", "1"))
indxTrain <- createDataPartition(y = pairs_data$Y, p = 0.8)
train1 <- pairs_data[indxTrain$Resample1,]

test <- pairs_data[-indxTrain$Resample1,]
train <- filter(train1, Y == 1)
non_interactions <- filter(train1, Y == 2)

train <- rbind(train, non_interactions[sample(nrow(non_interactions), nrow(train) + trunc(runif(1, 0.05 * nrow(train), 0.25 * nrow(train)))),])

print (paste0("Start Traning on ", train_chr))
col1 <- colnames(train)[c(5:82, 90:95)] 
mod <- randomForest(Y~., train[, col1])

print (paste0("Start Testing on ", train_chr))

pre <- predict(mod, newdata = test)
conf_mat <- confusionMatrix(pre, test$Y)
pro <- predict(mod, newdata = test, type = "prob")[,2]
pred <- prediction(pro, test$Y)
perf1 <- performance(pred,"auc")
print ("Confusion Matrix")
print (conf_mat)
print(paste("AUC: ", perf1@y.values[[1]]))


print (paste0("Start Testing the model Trained on ", train_chr,  " on the chromosome: ", test_chr))
train1 <- pairs_data

test <- readRDS(paste0(home_dir, "output_data/",cell_type,"_CTCF_ChIAPET/pairs_", test_chr, "_",cell_type,".RData" ))
test$Y <- as.factor(ifelse(as.character(test$Y) == "0", "2", "1"))
train <- filter(train1, Y == 1)
non_interactions <- filter(train1, Y == 2)

train <- rbind(train, non_interactions[sample(nrow(non_interactions), nrow(train) + trunc(runif(1, 0.05 * nrow(train), 0.25 * nrow(train)))),])

print (paste0("Start Traning on ", train_chr))
col1 <- colnames(train)[c(5:82, 90:95)] 
mod <- randomForest(Y~., train[, col1])

print (paste0("Start Testing on ", test_chr))

pre <- predict(mod, newdata = test)
conf_mat <- confusionMatrix(pre, test$Y)
pro <- predict(mod, newdata = test, type = "prob")[,2]
pred <- prediction(pro, test$Y)
perf1 <- performance(pred,"auc")
print ("Confusion Matrix")
print (conf_mat)
print(paste("AUC: ", perf1@y.values[[1]]))
rm(list = ls())